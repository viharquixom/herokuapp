
	// PAGE RELATED SCRIPTS

	/* chart colors default */
var $chrt_border_color = "#efefef";
var $chrt_grid_color = "#DDD"
var $chrt_main = "#E24913";
	/* red       */
var $chrt_second = "#6595b4";
	/* blue      */
var $chrt_third = "#FF9F01";
	/* orange    */
var $chrt_fourth = "#7e9d3a";
	/* green     */
var $chrt_fifth = "#BD362F";
	/* dark red  */
var $chrt_mono = "#000";

$(document).ready(function() {
pageSetUp();
initPie();
initBar();
populatecities();

});

function doAjax(url, type, data, contentType, dataType, div, basedon, successCallback, errorCallback)
{
	$.ajax({
		url : url,
		type : type,
		data : data,
		contentType : contentType,
		dataType : dataType,
		success : function(data){
			if(successCallback){
				successCallback(div, basedon, data);
			}
		},
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            //jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        },
		error : function(data){
			if(errorCallback){
				errorCallback(data);
			}
			
		} 

		
		
	});	
}

function initPie()
{
	
	$("#wid-id-0 header h2").text('Pie Chart Based On Age');
	$("#wid-id-1 header h2").text('Pie Chart Based On Course');
	$("#wid-id-2 header h2").text('Bar Chart Based On Recieved Application(Gender)');
	$("#wid-id-3 header h2").text('Bar Chart Based On Recieved Application(Course)');
	$("#wid-id-4 .appheader h2").text('Applications');
	$(".formheader h2").text('Submit Application');

	headerText = '<h4 class="header-text">Analysis Of Students Applying For Graduation</h4>'
	$("#ribbon").append(headerText);

	pieCharturl = '/api/v1/studentsp/' ;
	requestType = "GET";
	contentType = 'application/json';
	dataType= 'json';
	
	doAjax(pieCharturl, requestType, '', contentType, dataType, 'agepiechart', 'age', generatePieChartdata);
	doAjax(pieCharturl, requestType, '', contentType, dataType, 'coursepiechart', 'course', generatePieChartdata)
}

function initBar(){
	
		barCharturl = '/api/v1/studentsp/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		
		doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'gender', generatebarChartdata);
		doAjax(barCharturl, requestType, '', contentType, dataType, '#drawcoursebar', 'course', generatebarChartdata);
}

function generatePieChartdata(element,base,data)
{
	PiechartData = data ;
	PiechartData_length = PiechartData.objects.length;
	tempArray = [] ;
	dataArray = []
	
	if(base == 'age'){
		for(i=0;i<PiechartData_length;i++){
		tempArray.push(PiechartData.objects[i].age);
		}
	}else{
		for(i=0;i<PiechartData_length;i++){
		tempArray.push(PiechartData.objects[i].course);
		}
	}
	

	sortedArray = manageArray(tempArray);
	
	for(i=0;i<sortedArray[0].length;i++){
		 dataArray.push({'label' : sortedArray[0][i], 'value': sortedArray[1][i]});
	}
	
	drawPie(element,dataArray);
}

function generatebarChartdata(element, base,data)
{
	BarchartData = data ;
	BarchartData_length = BarchartData.objects.length;
	tempbarArray = [] ;
	databarArray = [] ;
	
	maleAppRecieved = [] ;
	femaleAppRecieved = [] ;
	
	maleAppCount = [] ;
	femaleAppCount =[] ; 

	scienceAppRecieved = [] ;
	commerceAppRecieved = [] ;
	artsAppRecieved = [] ;

	scienceAppCount = [] ;
	commerceAppCount = [] ;
	artsAppCount = [] ;
	
	barChartContent = []
	
	for(i=0;i<BarchartData_length;i++){
		timestamp = moment(BarchartData.objects[i].timestamp).format("MMM YYYY");
		var idx = $.inArray(timestamp, tempbarArray);
		if (idx == -1) {
			tempbarArray.push(timestamp);
		}
	}
	if(base == 'gender'){
		
		for(j=0;j<tempbarArray.length;j++){
			
			for(i=0;i<BarchartData_length;i++){
				timestamp = moment(BarchartData.objects[i].timestamp).format("MMM YYYY");
				if(timestamp == tempbarArray[j]){
					gender = BarchartData.objects[i].gender; 
					if(gender == "0"){
						femaleAppRecieved.push(gender);
					}else{
						maleAppRecieved.push(gender);
					}
				}
			}
			
			maleAppCount.push(maleAppRecieved.length);

			femaleAppCount.push(femaleAppRecieved.length);
			
			maleAppRecieved = [] ;
			femaleAppRecieved =[] ;
			
		}
		
		
		for(i=0;i<tempbarArray.length;i++){
			//barChartConent.push({"Field":tempbarArray[i],"Male":maleAppCount[i],"Female":femaleAppCount[i], "fields":[{ "field" : "Male" , "value" : maleAppCount[i]} , { "field" : "Female" , "value" : femaleAppCount[i]}]})
			barChartContent.push({"Field":tempbarArray[i],"Male":maleAppCount[i],"Female":femaleAppCount[i]});
			//console.log(maleAppCount[i]);
			//console.log(femaleAppCount[i]);
		}
	}else{
		
		for(j=0;j<tempbarArray.length;j++){
			
			for(i=0;i<BarchartData_length;i++){
				timestamp = moment(BarchartData.objects[i].timestamp).format("MMM YYYY");
				if(timestamp == tempbarArray[j]){
					course = BarchartData.objects[i].course;
					//console.log(course) 
					if(course == "Engineering"){
						scienceAppRecieved.push(course);
					}else if(course == "MBA"){
						commerceAppRecieved.push(course);
					}else{
						artsAppRecieved.push(course);
					}
				}
			}
			
			scienceAppCount.push(scienceAppRecieved.length);

			commerceAppCount.push(commerceAppRecieved.length);

			artsAppCount.push(artsAppRecieved.length);
			
			scienceAppRecieved = [] ;
			commerceAppRecieved =[] ;
			artsAppRecieved =[] ;
			
		}
		
		
		for(i=0;i<tempbarArray.length;i++){
			//barChartConent.push({"Field":tempbarArray[i],"Male":maleAppCount[i],"Female":femaleAppCount[i], "fields":[{ "field" : "Male" , "value" : maleAppCount[i]} , { "field" : "Female" , "value" : femaleAppCount[i]}]})
			barChartContent.push({"Field":tempbarArray[i],"Engineering":scienceAppCount[i],"MBA":commerceAppCount[i], "Pharmacy":artsAppCount[i]});
			
		}

	}
	drawbar(barChartContent, element);
	
}


function manageArray(array)
{
    var a = [], b = [], prev;
    
    array.sort();
    for ( var i = 0; i < array.length; i++ ) {
        if ( array[i] !== prev ) {
            a.push(array[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = array[i];
    }
    
    return [a, b];
}

function drawPie(element,chartContent)
{
	//console.log(chartContent);
	var pie = new d3pie(element, {
			size: {
				canvasHeight: 400,
				canvasWidth: 500,
				pieInnerRadius: 0,
				pieOuterRadius: null
			},
			data: {
				content: chartContent
			}
	});
}

function drawbar(graphcontent, elemDiv)
{
	//var barGraphdata = content ;
	
	barGraphdata = graphcontent	;
	
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 690 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;

var x0 = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var x1 = d3.scale.ordinal();

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.ordinal()
    .range(["#1d6a9a", "#51851a", "#7b6888", "#1d6a9a", "#1d6a9a", "#7b6888", "#1d6a9a"]);

var xAxis = d3.svg.axis()
    .scale(x0)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format(".2s"));

var svg = d3.select(elemDiv).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//d3.csv("data.csv", function(error, data) {
  var objectNames = d3.keys(barGraphdata[0]).filter(function(key) { return key !== "Field"; });
  //console.log(objectNames);
  
  //var oobjectNames = d3.keys(barGraphdata).filter(function(key) { return key !== "Field"; });
  //console.log(oobjectNames);

  barGraphdata.forEach(function(d) {
    d.fields = objectNames.map(function(field) { return {field: field, value: +d[field]}; });
  });

  x0.domain(barGraphdata.map(function(d) { return d.Field; }));
  x1.domain(objectNames).rangeRoundBands([0, x0.rangeBand()]);
  y.domain([0, d3.max(barGraphdata, function(d) { return d3.max(d.fields, function(d) { return d.value; }); })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Application");

  var state = svg.selectAll(".field")
      .data(barGraphdata)
    .enter().append("g")
      .attr("class", "g")
      .attr("transform", function(d) { return "translate(" + x0(d.Field) + ",0)"; });

  state.selectAll("rect")
      .data(function(d) { return d.fields; })
    .enter().append("rect")
      .attr("width", x1.rangeBand())
      .attr("x", function(d) { return x1(d.field); })
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); })
      .style("fill", function(d) { return color(d.field); });

  var legend = svg.selectAll(".legend")
      .data(objectNames.slice().reverse())
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });

//});
}	

function populatecities(){

		Cityurl = '/api/v1/city/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		
		doAjax(Cityurl, requestType, '', contentType, dataType, '', '', appendCities);
		
		$(".select-city").select2();
}

function appendCities(data){
	console.log(data);
}