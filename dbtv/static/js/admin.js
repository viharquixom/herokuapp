$(document).ready(function(){
headerText = '<h4 class="header-text">Analysis Of Students Applying For Graduation</h4>'
$("#ribbon").append(headerText);
$('.Barchart h2').text('Barchart');
$('.dt').text('Filtered values');
populatecast();
getSearch();

$(document).on('click', '.filterOp', setOption);
$(document).on('click', '.getSearch', getSearch);

});

$(window).load(function(){
$('.dt').text('Filtered values');

});

function getData(url, type, data, contentType, dataType,  successCallback, errorCallback)
{
	$.ajax({
		url : url,
		type : type,
		data : data,
		contentType : contentType,
		dataType : dataType,
		success : function(data){
			if(successCallback){
				successCallback(data);
			}
		},
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            //jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        },
		error : function(data){
			if(errorCallback){
				errorCallback(data);
			}
			
		} 

		
		
	});	
}

function doAjax(url, type, data, contentType, dataType, div, basedon, successCallback, errorCallback)
{
	$.ajax({
		url : url,
		type : type,
		data : data,
		contentType : contentType,
		dataType : dataType,
		success : function(data){
			$('.overlay').addClass('hidden');
			if(successCallback){
				successCallback(div, basedon, data);

			}
		},
		beforeSend: function(jqXHR, settings) {
			$('.overlay').removeClass('hidden');
            // Pull the token out of the DOM.
            //jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        },
		error : function(data){
			if(errorCallback){
				errorCallback(data);
			}
			
		} 

		
		
	});	
}

function setOption(){
	aid = $(this).attr('id');
	$('.filter-btn').attr('data-val', aid);
	$('.filter-btn').html(aid + '&nbsp;<span class="caret"></span>');

	if(aid == "Cast"){
		populatecast()
	}
	if(aid == "University"){
		populateuni();
	}
	if(aid == "City"){
		populatecities();
	}
	if(aid == "pctange"){
		populatemerit();
	}
	if(aid == "Passout"){
		populatepass();
	}
	if(aid == "Gap"){
		populategap();
	}
	if(aid == "Age"){
		populateage();
	}
	if(aid == "Course"){
		populatecourse();
	}
}

function populatecast(){
	castDir = ['Open', 'ST', 'SC']
	$(".search-project option").remove();
	for(i=0;i<castDir.length;i++){
		city = castDir[i]; 
		$('.search-project').append('<option selected="selected" value="'+ castDir[i] +'" data-uri="'+ castDir[i] +'">'+ castDir[i] +'</option>');
	}
	
}

function populatecourse(){
	courseDir = ['Engineering','MBA','Pharmacy'] 
	$(".search-project option").remove();
	for(i=0;i<courseDir.length;i++){
		course = courseDir[i]; 
		$('.search-project').append('<option selected="selected" value="'+ courseDir[i] +'" data-uri="'+ courseDir[i] +'">'+ courseDir[i] +'</option>');
	}
	
}

function populateage(){
	ageDir = ['20-22', '23-25', '26-28', '29-31']
	$(".search-project option").remove();
	for(i=0;i<ageDir.length;i++){
		age = ageDir[i]; 
		$('.search-project').append('<option selected="selected" value="'+ ageDir[i] +'" data-uri="'+ ageDir[i] +'">'+ ageDir[i] +'</option>');
	}
	
}

function populatecities(){

		Cityurl = '/api/v1/city/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		$(".search-project option").remove();
		getData(Cityurl, requestType, '', contentType, dataType, appendCities);
		
}

function populateuni(){

		Uniurl = '/api/v1/uni/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		$(".search-project option").remove();
		getData(Uniurl, requestType, '', contentType, dataType, appendUni);
		
}

function appendCities(data){
	console.log(data);
	citiesData =  data;
	citiesData_length = citiesData.objects.length;
	cityArraay = [] ;
	cityUriArraay = [] ;
	$(".search-project option").remove();
	for(i=0;i<citiesData_length;i++){
		city = citiesData.objects[i].name ; 
		uri = citiesData.objects[i].id ;
		var idx = $.inArray(city, cityArraay);
		if (idx == -1) {
			cityArraay.push(city);
			cityUriArraay.push(uri);
			$('.search-project').append('<option selected="selected" value="'+ uri +'" data-uri="'+ uri +'">'+ city +'</option>')
		}
	}

	
}

function appendUni(data){
	console.log(data);
	uniData =  data;
	uniData_length = uniData.objects.length;
	uniArraay = [] ;
	uniUriArraay = [] ;
	$(".search-project option").remove();
	for(i=0;i<uniData_length;i++){
		uni = uniData.objects[i].name ; 
		uniuri = uniData.objects[i].id ;
		var uidx = $.inArray(uni, uniArraay);
		if (uidx == -1) {
			uniArraay.push(uni);
			uniUriArraay.push(uniuri);
			$('.search-project').append('<option selected="selected" value="'+ uniuri +'" data-uri="'+ uniuri +'">'+ uni +'</option>')

		}
	}
}

function populatemerit(){
	meritDir = ['Less Than 55', '55-60', '61-70',  '71-80',  '81-90',  '91-100'];
	$(".search-project option").remove();
	for(i=0;i<meritDir.length;i++){
		merit = meritDir[i];
		$('.search-project').append('<option selected="selected" value="'+ meritDir[i] +'" data-uri="'+ meritDir[i] +'">'+ meritDir[i] +'</option>');
	}
	
}

function populatepass(){
	passDir = ['2008','2009', '2010', '2011','2012', '2013', '2014'];
	$(".search-project option").remove();
	for(i=0;i<passDir.length;i++){
		pass = passDir[i]; 
		$('.search-project').append('<option selected="selected" value="'+ passDir[i] +'" data-uri="'+ passDir[i] +'">'+ passDir[i] +'</option>');
	}
	
}

function populategap(){
	gapDir = ['No Gap','1 Year', '2 Year', '3 Year','4 Year'];
	$(".search-project option").remove();
	for(i=0;i<gapDir.length;i++){
		gap = gapDir[i]; 
		$('.search-project').append('<option selected="selected" value="'+ i +'" data-uri="'+ gapDir[i] +'">'+ gapDir[i] +'</option>');
	}

}

function getSearch(){

	base = $('.filter-btn').attr('data-val');
	basefn1(base);
}

function basefn1(filterby){

		appendParam = $('.search-project').val()
		
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		$('#drawbar').empty();
		

		if(filterby == "Cast"){
			barCharturl = '/api/v1/studentsp/?cast=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'personal', generatebarChartdata);
		}
		if(filterby == "University"){
			barCharturl = '/api/v1/studentse/?uni=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'education', generatebarChartdata);
		}
		if(filterby == "City"){
			barCharturl = '/api/v1/studentsp/?pcity=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'personal', generatebarChartdata);
		}
		if(filterby == "pctange"){
			if(appendParam == 'Less Than 55'){
				barCharturl = '/api/v1/studentse/?percentage__gte=0&percentage__lte=54'
			}else{
				startmerit = appendParam.substring(0,2);
				endmerit = appendParam.substring(3);
				barCharturl = '/api/v1/studentse/?percentage__gte='+ startmerit +'&percentage__lte='+ endmerit
			}
			
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'education', generatebarChartdata);
		}
		if(filterby == "Passout"){
			barCharturl = '/api/v1/studentse/?passout=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'education', generatebarChartdata);
		}
		if(filterby == "Gap"){
			barCharturl = '/api/v1/studentse/?gap=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'education', generatebarChartdata);
		}

		if(filterby == "Age"){
			startage = appendParam.substring(0,2);
			endage = appendParam.substring(3);
			barCharturl = '/api/v1/studentsp/?age__gte='+ startage +'&age__lte='+ endage
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'personal', generatebarChartdata);
		}
		if(filterby == "Course"){
			barCharturl = '/api/v1/studentsp/?course=' + appendParam ;
			doAjax(barCharturl, requestType, '', contentType, dataType, '#drawbar', 'personal', generatebarChartdata);
		}

		
		
		
}

function generatebarChartdata(element, base,data)
{
	BarchartData = data ;
	BarchartData_length = BarchartData.objects.length;
	tempbarArray = [] ;
	databarArray = [] ;
	
	maleAppRecieved = [] ;
	femaleAppRecieved = [] ;
	
	maleAppCount = [] ;
	femaleAppCount =[] ; 

	scienceAppCount = [] ;
	commerceAppCount = [] ;
	artsAppCount = [] ;
	
	barChartContent = []
	
	for(i=0;i<BarchartData_length;i++){
		if(base == 'personal'){
			timestamp = moment(BarchartData.objects[i].timestamp).format("MMM YYYY");
		}else{
			timestamp = moment(BarchartData.objects[i].student.timestamp).format("MMM YYYY");
		}
		
		var idx = $.inArray(timestamp, tempbarArray);
		if (idx == -1) {
			tempbarArray.push(timestamp);
		}
	}
	
	for(j=0;j<tempbarArray.length;j++){
		
		for(i=0;i<BarchartData_length;i++){
			if(base == 'personal'){
				timestamp = moment(BarchartData.objects[i].timestamp).format("MMM YYYY");
			}else{
				timestamp = moment(BarchartData.objects[i].student.timestamp).format("MMM YYYY");
			}
			
			if(timestamp == tempbarArray[j]){
				if(base == 'personal'){
					gender = BarchartData.objects[i].gender;
				}else{
					gender = BarchartData.objects[i].student.gender;
				}
				 
				if(gender == "0"){
					femaleAppRecieved.push(gender);
				}else{
					maleAppRecieved.push(gender);
				}
			}
		}
		
		maleAppCount.push(maleAppRecieved.length);

		femaleAppCount.push(femaleAppRecieved.length);
		
		maleAppRecieved = [] ;
		femaleAppRecieved =[] ;
			
		}

		for(i=0;i<tempbarArray.length;i++){
			//barChartConent.push({"Field":tempbarArray[i],"Male":maleAppCount[i],"Female":femaleAppCount[i], "fields":[{ "field" : "Male" , "value" : maleAppCount[i]} , { "field" : "Female" , "value" : femaleAppCount[i]}]})
			barChartContent.push({"Field":tempbarArray[i],"Male":maleAppCount[i],"Female":femaleAppCount[i]});
			//console.log(maleAppCount[i]);
			//console.log(femaleAppCount[i]);
		}
		
		maleApptotatal = 0;
		for(i=0;i<maleAppCount.length;i++){
			maleApptotatal += maleAppCount[i]			
		}

		femaleApptotatal = BarchartData_length - maleApptotatal ;
		
		$('.totalApp').text(BarchartData_length);
		$('.maleApp').text(maleApptotatal);
		$('.femaleApp').text(femaleApptotatal);

	drawbar(barChartContent, element);
	
}

function drawbar(graphcontent, elemDiv)
{
	//var barGraphdata = content ;
	
	barGraphdata = graphcontent	;
	
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 690 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;

var x0 = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var x1 = d3.scale.ordinal();

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.ordinal()
    .range(["#1d6a9a", "#51851a", "#7b6888", "#1d6a9a", "#1d6a9a", "#7b6888", "#1d6a9a"]);

var xAxis = d3.svg.axis()
    .scale(x0)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format(".2s"));

var svg = d3.select(elemDiv).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//d3.csv("data.csv", function(error, data) {
  var objectNames = d3.keys(barGraphdata[0]).filter(function(key) { return key !== "Field"; });
  //console.log(objectNames);
  
  //var oobjectNames = d3.keys(barGraphdata).filter(function(key) { return key !== "Field"; });
  //console.log(oobjectNames);

  barGraphdata.forEach(function(d) {
    d.fields = objectNames.map(function(field) { return {field: field, value: +d[field]}; });
  });

  x0.domain(barGraphdata.map(function(d) { return d.Field; }));
  x1.domain(objectNames).rangeRoundBands([0, x0.rangeBand()]);
  y.domain([0, d3.max(barGraphdata, function(d) { return d3.max(d.fields, function(d) { return d.value; }); })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Application");

  var state = svg.selectAll(".field")
      .data(barGraphdata)
    .enter().append("g")
      .attr("class", "g")
      .attr("transform", function(d) { return "translate(" + x0(d.Field) + ",0)"; });

  state.selectAll("rect")
      .data(function(d) { return d.fields; })
    .enter().append("rect")
      .attr("width", x1.rangeBand())
      .attr("x", function(d) { return x1(d.field); })
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); })
      .style("fill", function(d) { return color(d.field); });

  var legend = svg.selectAll(".legend")
      .data(objectNames.slice().reverse())
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });


}	