from django.conf.urls import patterns, include, url
from django.contrib import admin
from tastypie.api import Api
from reports.api import Uni, CityResource, StudentPResource, StudentEResource
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(Uni())
v1_api.register(CityResource())
v1_api.register(StudentPResource())
v1_api.register(StudentEResource())

urlpatterns = patterns('',
    
    url(r'^$', 'reports.views.home', name='home'),
    url(r'^generate', 'reports.views.generate', name='generate'),
    url(r'^Ed', 'reports.views.Ed', name='generateEd'),
    url(r'^ct', 'reports.views.ct', name='ct'),
    url(r'^guni', 'reports.views.guni', name='guni'),
    url(r'^submit', 'reports.views.submit', name='submit'),
    url(r'^adminpanel', 'reports.views.adminpanel', name='stats'),
    url(r'^notification', 'reports.views.sendNotification', name='notify'),
    url(r'^admin/', include(admin.site.urls)),
    (r'^api/', include(v1_api.urls)),
    
)
