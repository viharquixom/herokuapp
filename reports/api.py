from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from reports.models import StudentsPDetails,StudentsEDetails,StudentsCity,Uni
from tastypie.authorization import Authorization
from tastypie import fields,  utils
from tastypie.constants import ALL
from tastypie.validation import Validation
from datetime import datetime, timedelta       
import json
from django.http.response import HttpResponse

       
class Uni(ModelResource):
    
    class Meta:
        queryset = Uni.objects.all()
        resource_name = 'uni'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'name' : ALL,
        }
        authorization= Authorization()
        always_return_data = True
 
class CityResource(ModelResource):
    
    class Meta:
        queryset = StudentsCity.objects.all()
        resource_name = 'city'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'name' : ALL,
            'state' : ALL,
        }
        authorization= Authorization()
        always_return_data = True

class StudentPResource(ModelResource):
    pcity = fields.ForeignKey(CityResource, 'pcity', blank=True, null=True, full=True)
    class Meta:
        queryset = StudentsPDetails.objects.all()
        resource_name = 'studentsp'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'gender' : ALL,
            'course' : ALL,
            'pcity' : ALL,
            'age' : ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'cast' : ALL,
            'email' : ALL,
        }
        authorization= Authorization()
        always_return_data = True

    def hydrate(self, bundle, request=None):
        bundle.obj.timestamp = datetime.now()
        return bundle


class StudentEResource(ModelResource):
    student = fields.ForeignKey(StudentPResource, 'student', blank=True, null=True, full=True)
    ecity = fields.ForeignKey(CityResource, 'ecity', blank=True, null=True, full=True)
    uni = fields.ForeignKey(Uni, 'uni', blank=True, null=True, full=True)

    class Meta:
        queryset = StudentsEDetails.objects.all()
        resource_name = 'studentse'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'student' : ALL,
            'school' : ALL,
            'ecity' : ALL,
            'percentage' : ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'passout' : ALL,
            'uni' : ALL,
            'gap' : ALL,
        }
        ordering = ['percentage']
        authorization= Authorization()
        always_return_data = True
        


