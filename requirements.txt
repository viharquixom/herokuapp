Django==1.6.5
dj-database-url==0.3.0
dj-static==0.0.6
django-tastypie==0.12.1
gunicorn==19.1.1
python-dateutil==2.2
python-mimeparse==0.1.4
simplejson==3.6.5
six==1.8.0
static3==0.5.1
psycopg2==2.4.4


